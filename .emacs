;; .emacs

(when (require 'package nil t)
  (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
  (package-initialize)

  (custom-set-variables
   '(custom-safe-themes
     (quote
      ("6e86edff9c0826b2586453dbf096c7f4ed17a27902e8e439a58e550457234adb" default)))
   '(diff-switches "-u")
   '(inhibit-default-init t)
   '(package-selected-packages
     (quote
      (php-mode ghc web-beautify cuda-mode markdown-mode web-mode yasnippet ztree w3
		unicode-troll-stopper rudel rainbow-mode csv-mode auctex)))
   '(tab-stop-list (number-sequence 4 200 4))
   '(tab-width 4)))

(setq column-number-mode t)
(setq backward-delete-char-untabify-method nil)
(load-theme 'wombat-term)

(setq indent-tabs-mode t)
(global-set-key (kbd "TAB") 'self-insert-command)
(global-set-key (kbd "RET") 'newline-and-indent)

(linum-mode)

;; Replace
(global-set-key (kbd "M-s") 'replace-string)

;; Autocomplete
(global-set-key (kbd "M-/") 'hippie-expand)
(setq hippie-expand-try-functions-list
      '(try-expand-dabbrev try-expand-dabbrev-all-buffers try-complete-file-name-partially
			   try-complete-file-name try-expand-all-abbrevs try-expand-list try-expand-line))

;; YASnippet autocomplete
(when (require 'yasnippet nil t)
  (add-to-list 'load-path "~/.emacs.d/plugins/snippets")
  (yas/global-mode 1))

;; Easy PG (GPG/PGP)
(when (require 'epa-file nil t)
  (epa-file-enable))

;; JavaScript autoparser
(when (require 'web-beautify nil t) ;; Not necessary if using ELPA package
  (eval-after-load 'js2-mode
    '(define-key js2-mode-map (kbd "C-c b") 'web-beautify-js))
  (eval-after-load 'json-mode
    '(define-key json-mode-map (kbd "C-c b") 'web-beautify-js))
  (eval-after-load 'sgml-mode
    '(define-key html-mode-map (kbd "C-c b") 'web-beautify-html))
  (eval-after-load 'css-mode
    '(define-key css-mode-map (kbd "C-c b") 'web-beautify-css)))

;; Number incrementer (https://www.emacswiki.org/emacs/IncrementNumber)
(defun increment-number-at-point ()
  (interactive)
  (skip-chars-backward "0123456789")
  (or (looking-at "[0123456789]+")
	  (error "No number at point"))
  (replace-match (number-to-string (1+ (string-to-number (match-string 0))))))
(defun decrement-number-at-point ()
  (interactive)
  (skip-chars-backward "0123456789")
  (or (looking-at "[0123456789]+")
	  (error "No number at point"))
  (replace-match (number-to-string (1- (string-to-number (match-string 0))))))

(global-set-key (kbd "C-c +") 'increment-number-at-point)

;; C/C++ settings
;; tabs
(defun carter-c-hook ()
  (setq c-default-style "linux"
	c-basic-offset 4)
  (setq-default indent-tabs-mode t)
  (setq-default indent-relative t)
  (setq-default newline-and-indent t))
(add-hook 'c-mode-common-hook 'carter-c-hook)
;; add prints automatically
(defun carter-add-debug-prints ()
  (interactive)
  (c-beginning-of-defun)
  (setq code "A")
  (while (search-forward "\n\n" (save-excursion (end-of-defun) (point)) t)
	(replace-match (concat "\ncout << \"" code "\\n\"\n") nil t)
	(setq code (char-to-string (+ 1 (string-to-char code)))))
  )

;; Bind spell check to C-M-i for mode modes
(defun text-hook ()
  (local-set-key (kbd "C-M-i") 'ispell)  
  (visual-line-mode t))
(add-hook 'text-mode-hook 'text-hook)

;; No tabs in python
(defun pythontab-hook ()
  (setq indent-tabs-mode nil))
(add-hook 'python-mode-hook 'pythontab-hook)

(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
  backup-by-copying nil    ; Don't delink hardlinks
  version-control t      ; Use version numbers on backups
  delete-old-versions t  ; Automatically delete excess backups
  kept-new-versions 0   ; how many of the newest versions to keep
  kept-old-versions 0    ; and how many of the old
  )

;; LaTeX
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq global-prettify-symbols-mode t)
(setq-default TeX-master nil)

;; Markdown
(defun firefox-remote-profile-browse (url &optional new-window)
  (interactive)
  (start-process "firefox" nil "firefox" "-P" "emacs" url))
  
(defun markdown-html-preview ()
  (interactive)
  (setq browse-url-backup browse-url-browser-function)
  (setq browse-url-browser-function 'firefox-remote-profile-browse)
  (markdown-preview)
  (setq browse-url-browser-function browse-url-backup))
  
(defun markdown-hook ()
;  (start-process "firefox" nil "firefox" "-no-remote" "-P" "emacs")
  (local-set-key (kbd "C-c C-c") 'markdown-html-preview))

(add-hook 'markdown-mode-hook 'markdown-hook)

; Set various filetypes
(add-to-list 'auto-mode-alist '("\\.ino\\'" . c++-mode)) ; Arduino is basically C++
(add-to-list 'auto-mode-alist '("\\.cu\\'" . c++-mode)) ; CUDA is closest to C++
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode)) ; I use .h for C++ headers
(add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode)) ; Matlab is basically Octave
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode)) ; I like putting JavaScript into HTML
(add-to-list 'auto-mode-alist '("\\.g4\\'" . antlr-mode))

(setq initial-scratch-message ";Scratch buffer (C-x C-e to add functions/libs, C-j to run\n(require 'cl)")
; (global-set-key (kbd "M-t") (lambda () (interactive) (print (buffer-substring (progn (beginning-of-line) (point)) (progn (end-of-line) (point))))))
(put 'downcase-region 'disabled nil)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'upcase-region 'disabled nil)
