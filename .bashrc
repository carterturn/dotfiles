# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus

export EDITOR=emacs
export VISUAL=emacs

export TERMINAL=/usr/bin/xterm

#中文
alias 写='emacs'
alias 去='cd'
alias 看='ls'

alias clipboard_copy='xclip -selection clipboard'
alias clipboard_paste='xclip -selection clipboard -o'

dir(){
	pwd | sed 's|/|\
|g' | tail -n1
}

xkcd(){
    firefox https://xkcd.com/$1
}

sensor(){
    sensors
    nvidia-smi
}

groot(){
    git status 1>/dev/null 2>/dev/null 
    if [[ $? == 0 ]];
    then
	cd $(git rev-parse --show-toplevel)
    else
	cd
    fi
}

play(){
	ffmpeg -i "$1" -f wav - | aplay -f cd -
}

git-all(){
	for folder in $(find . -type d -name .git);
	do
		cd $folder/../;
		echo $folder;
		git $1;
		echo "#####################################";
		cd -;
	done
}

timer(){
    date1=`date +%s`; while true; do echo -ne "$(date -u --date @$((`date +%s` - $date1)) +%H:%M:%S)\r"; done
}

iplocate(){
    curl freegeoip.net/csv/$1 2>/dev/null | sed 's/,/ /g'
}

gpu-lights(){
    sudo nvidia-settings -a [gpu:0]/GPULogoBrightness=$1 > /dev/null 2>&1
}

duo-code(){
	cd ~/Applications/System_Tools/duo_2fa/duo-bypass
	venv/bin/python duo_gen.py ~/Applications/System_Tools/duo_2fa/duo-bypass/duotoken.hotp | clipboard_copy
}

PATH=$PATH:$HOME/.local/bin
export LD_LIBRARY_PATH=/usr/lib:/usr/lib64:/usr/local/lib:/usr/local/lib64:.

###gpg2
export GPG_TTY=$(tty)
export GPG2_TTY=$(tty)

###MariaDB
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib64/mariadb

###Android
export PATH=${PATH}:/home/carter/Applications/Programming/android-platform-tools

###MATLAB
export PATH=$PATH:/home/carter/Applications/Programming/matlab/bin
alias matlab-nox='matlab -nodesktop -nosplash'

# Qt
export QT_PLUGIN_PATH=$QT_PLUGIN_PATH:/usr/lib/x86_64-linux-gnu/qt5/plugins

# Keep aliases and stuff with sudo (actually mostly for emacs)
xhost +si:localuser:root 1>/dev/null 2>/dev/null
alias sudo='sudo -E env PATH=$PATH'

if [ -z "$SSH_CLIENT" ] && [ -z "$SSH_TTY" ]; then
  cat ~/todo.txt
fi
